import { Select } from 'antd'

export default function Root(props) {
  return <main className="bg-gray-100 h-screen" >
    <section className="container mx-auto pt-20" >
      <div className="flex w-full gap-5">
        <div className="w-1/2">
          <h3>Listado de especialidades médicas</h3>
        </div>
        <div className="w-1/2 flex flex-col gap-5 ">
          <Select
            defaultValue="cardiologia"
            style={{ width: 120 }}
            options={[
              { value: 'cardiologia', label: 'Cardiología' },
              { value: 'ginecologia', label: 'Ginecología' },
              { value: 'ondontologia', label: 'Ondontología' },
            ]}
          />
        </div>
      </div>
    </section>
  </main>
}
